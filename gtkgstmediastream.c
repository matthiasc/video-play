/*
 * Copyright © 2018 Benjamin Otte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Benjamin Otte <otte@gnome.org>
 */

#include "gtkgstmediastreamprivate.h"
#include "gtkgstpaintableprivate.h"
#include "gtkgstsinkprivate.h"

#include <gst/gst.h>

struct _GtkGstMediaStream
{
  GtkMediaStream parent_instance;

  GstElement *pipeline;
  GdkPaintable *paintable;
  GstClockID timeout;
};

struct _GtkGstMediaStreamClass
{
  GtkMediaStreamClass parent_class;
};

#define TO_GST_TIME(ts) ((ts) * (GST_SECOND / G_USEC_PER_SEC))
#define FROM_GST_TIME(ts) ((ts) / (GST_SECOND / G_USEC_PER_SEC))

static void
gtk_gst_media_stream_paintable_snapshot (GdkPaintable *paintable,
                                         GdkSnapshot  *snapshot,
                                         double        width,
                                         double        height)
{
  GtkGstMediaStream *self = GTK_GST_MEDIA_STREAM (paintable);

  gdk_paintable_snapshot (self->paintable, snapshot, width, height);
}

static GdkPaintable *
gtk_gst_media_stream_paintable_get_current_image (GdkPaintable *paintable)
{
  GtkGstMediaStream *self = GTK_GST_MEDIA_STREAM (paintable);

  return gdk_paintable_get_current_image (self->paintable);
}

static int
gtk_gst_media_stream_paintable_get_intrinsic_width (GdkPaintable *paintable)
{
  GtkGstMediaStream *self = GTK_GST_MEDIA_STREAM (paintable);

  return gdk_paintable_get_intrinsic_width (self->paintable);
}

static int
gtk_gst_media_stream_paintable_get_intrinsic_height (GdkPaintable *paintable)
{
  GtkGstMediaStream *self = GTK_GST_MEDIA_STREAM (paintable);

  return gdk_paintable_get_intrinsic_height (self->paintable);
}

static double
gtk_gst_media_stream_paintable_get_intrinsic_aspect_ratio (GdkPaintable *paintable)
{
  GtkGstMediaStream *self = GTK_GST_MEDIA_STREAM (paintable);

  return gdk_paintable_get_intrinsic_aspect_ratio (self->paintable);
};

static void
gtk_gst_media_stream_paintable_init (GdkPaintableInterface *iface)
{
  iface->snapshot = gtk_gst_media_stream_paintable_snapshot;
  iface->get_current_image = gtk_gst_media_stream_paintable_get_current_image;
  iface->get_intrinsic_width = gtk_gst_media_stream_paintable_get_intrinsic_width;
  iface->get_intrinsic_height = gtk_gst_media_stream_paintable_get_intrinsic_height;
  iface->get_intrinsic_aspect_ratio = gtk_gst_media_stream_paintable_get_intrinsic_aspect_ratio;
}

G_DEFINE_TYPE_EXTENDED (GtkGstMediaStream, gtk_gst_media_stream, GTK_TYPE_MEDIA_STREAM, 0,
                        G_IMPLEMENT_INTERFACE (GDK_TYPE_PAINTABLE,
                                               gtk_gst_media_stream_paintable_init))

typedef struct {
  GtkGstMediaStream *stream;
  GstClockTime time;
} TimeoutData;

static gboolean
update_time (gpointer user_data)
{
  TimeoutData *data = user_data;

  if (GST_STATE (data->stream->pipeline) == GST_STATE_PLAYING)
    {
      GstClockTime time;

      time = data->time - gst_element_get_base_time (GST_ELEMENT (data->stream->pipeline));
      gtk_media_stream_update (GTK_MEDIA_STREAM (data->stream), FROM_GST_TIME (time));
    }
  return G_SOURCE_REMOVE;
}

static gboolean
timeout_cb (GstClock     *clock,
            GstClockTime  time,
            GstClockID    id,
            gpointer      user_data)
{
  TimeoutData *data = user_data;

  data->time = time;
  g_idle_add (update_time, data);
  return FALSE;
}

static gboolean
gtk_gst_media_stream_play (GtkMediaStream *stream)
{
  GtkGstMediaStream *self = GTK_GST_MEDIA_STREAM (stream);
  GstClockTime start_time;
  GstClock *clock;
  TimeoutData *data;

  if (self->pipeline == NULL)
    return FALSE;

  gst_element_set_state (GST_ELEMENT (self->pipeline), GST_STATE_PLAYING);
  start_time = gst_element_get_start_time (GST_ELEMENT (self->pipeline));
  clock = gst_pipeline_get_clock (GST_PIPELINE (self->pipeline));
  self->timeout = gst_clock_new_periodic_id (clock, start_time, GST_SECOND / 2);

  data = g_new0 (TimeoutData, 1);
  data->stream = self;
  gst_clock_id_wait_async (self->timeout, timeout_cb, data, g_free);

  return TRUE;
}

static void
gtk_gst_media_stream_pause (GtkMediaStream *stream)
{
  GtkGstMediaStream *self = GTK_GST_MEDIA_STREAM (stream);

  gst_clock_id_unschedule (self->timeout);
  g_clear_pointer (&self->timeout, gst_clock_id_unref);
  gst_element_set_state (GST_ELEMENT (self->pipeline), GST_STATE_PAUSED);
}

static void
gtk_gst_media_stream_update_audio (GtkMediaStream *stream,
                                   gboolean        muted,
                                   double          volume)
{
 // GtkGstMediaStream *self = GTK_GST_MEDIA_STREAM (stream);

  //gst_player_set_mute (self->player, muted);
  //gst_player_set_volume (self->player, volume);
}

static void
gtk_gst_media_stream_dispose (GObject *object)
{
  GtkGstMediaStream *self = GTK_GST_MEDIA_STREAM (object);

  if (self->paintable)
    {
      g_signal_handlers_disconnect_by_func (self->paintable, gdk_paintable_invalidate_size, self);
      g_signal_handlers_disconnect_by_func (self->paintable, gdk_paintable_invalidate_contents, self);
      g_clear_object (&self->paintable);
    }

  gst_clock_id_unschedule (self->timeout);
  g_clear_pointer (&self->timeout, gst_clock_id_unref);
  gst_element_set_state (GST_ELEMENT (self->pipeline), GST_STATE_NULL);

  g_clear_pointer (&self->pipeline, gst_object_unref);

  G_OBJECT_CLASS (gtk_gst_media_stream_parent_class)->dispose (object);
}

static void
gtk_gst_media_stream_class_init (GtkGstMediaStreamClass *klass)
{
  GtkMediaStreamClass *stream_class = GTK_MEDIA_STREAM_CLASS (klass);
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  stream_class->play = gtk_gst_media_stream_play;
  stream_class->pause = gtk_gst_media_stream_pause;
  stream_class->update_audio = gtk_gst_media_stream_update_audio;

  gobject_class->dispose = gtk_gst_media_stream_dispose;
}

static void
gtk_gst_media_stream_init (GtkGstMediaStream *self)
{
  self->paintable = gtk_gst_paintable_new ();
  g_signal_connect_swapped (self->paintable, "invalidate-size", G_CALLBACK (gdk_paintable_invalidate_size), self);
  g_signal_connect_swapped (self->paintable, "invalidate-contents", G_CALLBACK (gdk_paintable_invalidate_contents), self);
}

static void
create_pipeline_for_pipewire_fd (GtkGstMediaStream *self,
                                 int                fd,
                                 const char        *node)
{
  GstElement *source;
  GstElement *sink;
  GstElement *convert;
  GstElement *pipeline;

  source = gst_element_factory_make ("pipewiresrc", "pipewiresrc");

  if (fd != -1)
    g_object_set (source, "fd", fd, NULL);
  if (node != NULL)
    g_object_set (source, "path", node, NULL);

  convert = gst_element_factory_make ("videoconvert", "videoconvert");
  sink = g_object_new (GTK_TYPE_GST_SINK,
                       "paintable", self->paintable,
                       NULL);

  pipeline = gst_pipeline_new ("pipeline");
  gst_bin_add_many (GST_BIN (pipeline), source, convert, sink, NULL);

  if (!gst_element_link_many (source, convert, sink, NULL))
    {
      g_printerr ("Elements could not be linked.\n");
      return;
    }

  self->pipeline = g_object_ref_sink (pipeline);

  gtk_media_stream_prepared (GTK_MEDIA_STREAM (self), FALSE, TRUE, FALSE, 0);
}

GtkMediaStream *
gtk_gst_media_stream_new_for_pipewire_fd (int         fd,
                                          const char *node)
{
  GtkGstMediaStream *stream;

  stream = g_object_new (GTK_TYPE_GST_MEDIA_STREAM, NULL);

  create_pipeline_for_pipewire_fd (stream, fd, node);

  return GTK_MEDIA_STREAM (stream);
}
