#include <gtk/gtk.h>
#include <gst/gst.h>
#include <pipewire/pipewire.h>
#include <pipewire/global.h>

#include "gtkgstsinkprivate.h"
#include "gtkgstpaintableprivate.h"
#include "gtkgstmediastreamprivate.h"

#include "libportal/portal.h"

static XdpPortal *portal;

static void
got_camera (GObject *source,
            GAsyncResult *result,
            gpointer data)
{
  XdpPortal *portal = XDP_PORTAL (source);
  GError *error = NULL;
  int fd;
  GtkMediaStream *stream;

  if (!xdp_portal_access_camera_finish (portal, result, &error))
    {
      g_printerr ("Dangit! No camera access\n%s", error->message);
      g_clear_error (&error);
      return;
    }

  fd = xdp_portal_open_pipewire_remote_for_camera (portal);

  stream = gtk_gst_media_stream_new_for_pipewire_fd (fd, NULL);
  gtk_video_set_media_stream (GTK_VIDEO (data), stream);
  g_object_unref (stream);

  close (fd);
}

static void
clear_video (GtkWidget *video)
{
  gtk_video_set_media_stream (GTK_VIDEO (video), NULL);
}

static void
connect_camera (GtkWidget *pictures)
{
  xdp_portal_access_camera (portal, NULL, 0, NULL, got_camera, pictures);
}

static void
close_session (gpointer data)
{
  xdp_session_close (XDP_SESSION (data));
  g_object_unref (data);
}

static void
session_started (GObject *source,
                 GAsyncResult *result,
                 gpointer data)
{
  XdpSession *session = XDP_SESSION (source);
  GError *error = NULL;
  int fd;
  GVariant *streams;
  GVariantIter *iter;
  guint node_id;
  GVariant *value;

  if (!xdp_session_start_finish (session, result, &error))
    {
      g_printerr ("Session start failed :(\n%s", error->message);
      g_clear_error (&error);
      return;
    }

  fd = xdp_session_open_pipewire_remote (session);

  streams = xdp_session_get_streams (session);
  g_print ("Streams: %s\n", g_variant_print (streams, FALSE));

  iter = g_variant_iter_new (streams);
  while (g_variant_iter_next (iter, "(u@a{sv})", &node_id, &value))
    {
      char *path;
      GtkMediaStream *stream;

      path = g_strdup_printf ("%u", node_id);
      stream = gtk_gst_media_stream_new_for_pipewire_fd (fd, path);
      g_object_set_data_full (G_OBJECT (stream), "session", session, close_session);
      gtk_video_set_media_stream (GTK_VIDEO (data), stream);
      g_object_unref (stream);

      g_variant_unref (value);
      g_free (path);
      break;
    }
  g_variant_iter_free (iter);

  close (fd);
}

static void
got_session (GObject *source,
             GAsyncResult *result,
             gpointer data)
{
  XdpPortal *portal = XDP_PORTAL (source);
  XdpSession *session;
  GError *error = NULL;

  session = xdp_portal_create_screencast_session_finish (portal, result, &error);
  if (!session)
    {
      g_printerr ("Dangit! No screencast access\n%s", error->message);
      g_clear_error (&error);
      return;
    }

  xdp_session_start (session, NULL, NULL, session_started, data);
}

static void
connect_screen (GtkWidget *pictures)
{
  xdp_portal_create_screencast_session (portal,
                                        XDP_OUTPUT_WINDOW | XDP_OUTPUT_MONITOR,
                                        XDP_SCREENCAST_FLAG_NONE,
                                        NULL,
                                        got_session,
                                        pictures);
}

static gboolean
quit_cb (GtkWindow *window,
         gpointer   data)
{
  *((gboolean *)data) = TRUE;

  g_main_context_wakeup (NULL);

  return TRUE;
}

int
main (int argc, char *argv[])
{
  GtkWidget *window;
  GtkWidget *video;
  GtkWidget *box, *box2, *button;
  gboolean done = FALSE;

  gtk_init ();
  pw_init (NULL, NULL);
  gst_init (NULL, NULL);

  window = gtk_window_new ();
  gtk_window_set_default_size (GTK_WINDOW (window), 640, 480);
  gtk_window_set_title (GTK_WINDOW (window), "GtkVideo");

  box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 10);
  gtk_window_set_child (GTK_WINDOW (window), box);
  video = gtk_video_new ();
  gtk_widget_set_halign (video, GTK_ALIGN_FILL);
  gtk_widget_set_valign (video, GTK_ALIGN_FILL);
  gtk_widget_set_hexpand (video, TRUE);
  gtk_widget_set_vexpand (video, TRUE);
  gtk_box_append (GTK_BOX (box), video);

  gtk_video_set_autoplay (GTK_VIDEO (video), TRUE);

  box2 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
  gtk_box_append (GTK_BOX (box), box2);
  button = gtk_button_new_with_label ("Camera");
  gtk_widget_set_hexpand (button, TRUE);
  g_signal_connect_swapped (button, "clicked",
                            G_CALLBACK (connect_camera), video);
  gtk_box_append (GTK_BOX (box2), button);
  button = gtk_button_new_with_label ("Screen");
  gtk_widget_set_hexpand (button, TRUE);
  g_signal_connect_swapped (button, "clicked",
                            G_CALLBACK (connect_screen), video);
  gtk_box_append (GTK_BOX (box2), button);
  button = gtk_button_new_with_label ("Stop");
  gtk_widget_set_hexpand (button, TRUE);
  g_signal_connect_swapped (button, "clicked",
                            G_CALLBACK (clear_video), video);
  gtk_box_append (GTK_BOX (box2), button);

  gtk_window_present (GTK_WINDOW (window));

  portal = xdp_portal_new ();

  g_signal_connect (window, "close-request", G_CALLBACK (quit_cb), &done);

  while (!done)
    g_main_context_iteration (NULL, TRUE);

  gtk_window_destroy (GTK_WINDOW (window));

  return 0;
}
